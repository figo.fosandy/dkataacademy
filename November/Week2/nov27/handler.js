const {Trainee}=require('./components/academy')
const mawar=new Trainee('Mawar','Melati',19,'Female','Single','Police','Jakarta','Reading','Javascript')
const ijah=new Trainee('Ijah','Melati',17,'Female','Single','Neet','Bogor','Watching','C++')
const trainees=[mawar,ijah]

const getTime=time=>{
    const convertedTime=new Date(time)
    return {
        hour:convertedTime.getHours(),
        minute:convertedTime.getMinutes(),
        second:convertedTime.getSeconds(),
    }
}

const rootHandler=(request,h)=>{
    return 'This is a root route'
}

const nameHandler=(request,h)=>{
    return `Hello ${request.params.name}`
}

const traineesHandler=(request,h)=>{
    return trainees
}

const parseTimeHandler=(request,h)=>{
    return getTime(request.query.iso)
}

module.exports={rootHandler,nameHandler,traineesHandler,parseTimeHandler,trainees}