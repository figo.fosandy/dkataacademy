const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
const {trainees}=require('../handler')

describe('GET',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.statusCode).to.equal(200)
    })
    
    it('response result for path "/" is "This is a root route"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.result).to.equal("This is a root route")
    })
    
    it('response statusCode for path "/api/v1/hello/figo" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/hello/figo'
        })
        expect(res.statusCode).to.equal(200 )
    })

    it('response params for path "/api/v1/hello/figo" is name:"figo"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/hello/figo'
        })
        expect(res.request.params.name).to.equal('figo')
    })
    
    it('response result for path "/api/v1/hello/figo" is Hi figo',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/hello/figo'
        })
        expect(res.result).to.equal(`Hello ${res.request.params.name}`)
    })
    
    it('response statusCode for path "/api/v1/hello/figo1" is 400',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/hello/figo1'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response result for path "/api/v1/hello/figo1" is error detail message',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/hello/figo1'
        })
        expect(res.result).to.equal({
            statusCode:400,
            error:'Bad Request',
            message:'Invalid request params input'
        })
    })
    
    it('response statusCode for path "/api/v1/trainees" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/trainees'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('response path "/api/v1/trainees" result is list of trainee',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/trainees'
        })
        expect(res.result).to.equal(trainees)
    })

    it('response statusCode for path "/api/v1/parsetime" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/parsetime'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('response query for path "/api/v1/parsetime?iso=8:24 PM 11/27/2019" is {iso:8:24 PM 11/27/2019}',async()=>{
        const time='8:24 PM 11/27/2019'
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/parsetime?iso=8:24 PM 11/27/2019'
        })
        expect(res.request.query.iso).to.equal('8:24 PM 11/27/2019')
    })

    it('response result for path "/api/v1/parsetime?iso=8:24 PM 11/27/2019" is {"hour": 20,"minute": 24,"second": 0}',async()=>{
        const time='8:24 PM 11/27/2019'
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/parsetime?iso=8:24 PM 11/27/2019'
        })
        expect(res.result).to.equal(
            {
                "hour": 20,
                "minute": 24,
                "second": 0
            }
        )
    })

    it('response statusCode for path "/api/v1/images/sample.txt" is 400',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/images/sample.txt'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/api/v1/images/maximize_icon.png" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/api/v1/images/maximize_icon.png'
        })
        expect(res.statusCode).to.equal(200)
    })
})