const pets=require('./pets.json')

const getsPetSortOffsetLimit=(sortBy,offset,limit)=>{
    if(typeof pets[0][sortBy] == 'string') {
        result=pets.sort((a,b)=>a[sortBy]>b[sortBy])
    } else if(typeof pets[0][sortBy] == 'number') {
        result=pets.sort((a,b)=>a[sortBy]-b[sortBy])
    }
    return pets.slice(offset,offset+limit)
}

const getPets=()=>{
    return pets
}

const getPetDetail=(id)=>{
    return pets.filter(pet=>pet.id==id)[0]
}

const addPet=(pet)=>{
    let ids=pets.map(pet=>pet.id)
    let lastId=Math.max(...ids)    
    let newPet=Object.assign({id:lastId+1},pet)
    pets.push(newPet)
    return newPet
}

const updatePet=(id,pet)=>{
    Object.assign(pets.find(pet=>pet.id==id),pet)
    return pets.find(pet=>pet.id==id)
}

module.exports={getsPetSortOffsetLimit,getPets,getPetDetail,addPet,updatePet}