const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {pets,start}=require('../server')

describe('GET',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.statusCode).to.equal(200)
    })
    
    it('response result for path "/" is "This is a root route"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.result).to.equal("This is a root route")
    })
    
    it('response statusCode for path "/pets?query" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets?sort=id&offset=0&limit=100'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('response statusCode for path "/pets?sort=names" is 404',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets?sort=names'
        })
        expect(res.statusCode).to.equal(404)
    })

    it('response statusCode for path "/pets?sort=name1" is 400',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets?sort=name1'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response result for path "/pets" is array containing pets',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets'
        })
        expect(res.result).to.equal(pets)
    })

    it('response result for path "/pets/1" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/1'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('response result for path "/pets/1" is object containing id 1',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/1'
        })
        expect(res.result).to.equal(pets[0])
    })

    it('response statusCode for path "/pets/a" is 400',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/a'
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response result for path "/pets/a" is error Detail',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/a'
        })
        expect(res.result.error).to.equal('Bad Request')
    })

    it('response  for path "/pets" method "POST"',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            payload:{
                name:'bonjo',
                breed:'local',
                colour:'invisible',
                age:2,
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(201)
    })

    it('response statusCode for path "/pets" method "POST" with invalid payload is 400',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            payload:{
                id:1,
                name:'bonjo',
                breed:'local',
                colour:'invisible',
                age:2,
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" method "POST" with conflict payload is 409',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            payload:{
                name:'tommy',
                breed:'local',
                colour:'invisible',
                age:2,
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(409)
    })

    it('response statusCode for path "/pets/1" method "PUT" with right payload is 200',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            payload:{
                name:'tommy',
                breed:'local',
                colour:'invisible',
                age:2,
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(202)
    })

    it('response statusCode for path "/pets/1" method "PUT" with invalid payload is 400',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            payload:{
                name:'tommy',
                breed:'local1',
                colour:'invisible',
                age:2,
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets/1" method "PUT" with conflict payload is 409',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            payload:{
                name:'bonzo',
                breed:'local',
                colour:'invisible',
                age:'2',
                next_checkup:'2019-1-1',
                vaccinations:[
                    'tetanus',
                    'polio'
                ]
            }
        })
        expect(res.statusCode).to.equal(409)
    })
})