const {addPet,getPetDetail,getPets,getsPetSortOffsetLimit,updatePet}=require('./petsQuery')

const rootHandler=(request,h)=>{
    return h.response('This is a root route').code(200)
}

const petsGetHandler=(request,h)=>{
    if(!request.query.sort&&!request.query.offset&&!request.query.limit){
        return h.response(getPets()).code(200)
    }
    let sortBy=request.query.sort?request.query.sort:'id'
    let offset=request.query.offset?parseInt(request.query.offset):0
    let limit=request.query.limit?parseInt(request.query.limit):10
    if(!getPetDetail(1)[sortBy]){
        return h.response({
            statusCode:404,
            error:'Not Found',
            message:'sort query not found'
        }).code(404)
    }
    return h.response(getsPetSortOffsetLimit(sortBy,offset,limit)).code(202)
}

const petsDetailGetHandler=(request,h)=>{
    const id=request.params.id
    return h.response(getPetDetail(id)).code(200)
}

const petsPostHandler=(request,h)=>{
    let names=getPets().map(pet=>pet.name)
    let newPetName=request.payload.name
    let nameAvailability=names.some(name=>name==newPetName)
    if(nameAvailability) {
        return h.response({
            statusCode:409,
            error:'Conflict',
            message:'Name is conflict'
        }).code(409)
    }
    return h.response(addPet(request.payload)).code(201)
}

const petsPutHandler=(request,h)=>{
    let id=request.params.id
    let name=getPetDetail(id).name
    if(request.payload.name!=name||(request.payload.id&&request.payload.id!=id)){
        return h.response({
            statusCode:409,
            error:'Conflict',
            message:'Id or Name is conflict'
        }).code(409)
    }
    return h.response(updatePet(id,request.payload)).code(202)
}

module.exports={rootHandler,petsGetHandler,petsDetailGetHandler,petsPostHandler,petsPutHandler}