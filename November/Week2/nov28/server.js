const Hapi=require('@hapi/hapi')
const Joi=require('@hapi/joi')
const {rootHandler,petsGetHandler,petsDetailGetHandler,petsPostHandler,petsPutHandler}=require('./handler')

const start=async()=>{
    const server=Hapi.server({
        port:1128,
        host:'localhost'
    })
        
    server.route({
        method:'GET',
        path:'/',
        handler:rootHandler
    })
    
    server.route({
        method:'GET',
        path:'/pets',
        handler:petsGetHandler,
        options:{
            validate:{
                query:{
                    sort:Joi.string().regex(/^[a-z_]+$/),
                    offset:Joi.number().integer().min(0),
                    limit:Joi.number().integer().min(1)
                }
            }
        }
    })

    server.route({
        method:'GET',
        path:'/pets/{id}',
        handler:petsDetailGetHandler,
        options:{
            validate:{
                params:{
                    id:Joi.number().integer()
                }
            }
        }
    })

    server.route({
        method:'POST',
        path:'/pets',
        handler:petsPostHandler,
        options:{
            validate:{
                payload:{
                    id:Joi.forbidden(),
                    name:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    breed:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    colour:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    age:Joi.number().integer().min(0).required(),
                    next_checkup:Joi.date().required(),
                    vaccinations:Joi.array().required()
                }
            }
        }
    })

    server.route({
        method:'PUT',
        path:'/pets/{id}',
        handler:petsPutHandler,
        options:{
            validate:{
                payload:{
                    id:Joi.number().integer(),
                    name:Joi.string().required(),
                    breed:Joi.string().regex(/^[a-zA-Z\s]+$/),
                    colour:Joi.string().regex(/^[a-zA-Z\s]+$/),
                    age:Joi.number().integer().min(0),
                    next_checkup:Joi.date(),
                    vaccinations:Joi.array()
                }
            }
        }
    })

    await server.start()
    
    console.log(`Server running on ${server.info.uri}`)
    
    process.on('unhandledRejection',(err)=>{
        console.log(err)
        process.exit(1)
    })
    
    return server
}

module.exports={start}