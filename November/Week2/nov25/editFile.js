const fs=require('fs')
const {Transform}=require('stream')

const uncapitalData=process.argv[2]
const capitalData=process.argv[3]

String.prototype.capitalize=function(){
    return this.replace(/\s*\w/, x=>x.toUpperCase()).replace(/\.\s*\w/g,x=>x.toUpperCase())
}

const capitilizing=(uncapitalizedPath,capitalizedPath)=>{
    const uncapitalizedData=uncapitalData?uncapitalData:uncapitalizedPath
    const capitilizedData=capitalData?capitalData:capitalizedPath

    const readStream=fs.createReadStream(uncapitalizedData)
    const writeStream=fs.createWriteStream(capitilizedData)

    const CapitalizeTransform=new Transform({
        transform(chunck,enc,done){
            this.push(chunck.toString().capitalize())
            done()
        }
    })
    readStream.pipe(CapitalizeTransform).pipe(writeStream)
}

module.exports=capitilizing
if(uncapitalData){
    capitilizing()
    fs.readFile(capitalData,'utf8',(err,data)=>console.log(data))
}