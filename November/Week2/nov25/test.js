const findImage=require('./findImage')
const editFile=require('./editFile')
const fs=require('fs')

test('Find Image',()=>{
    expect(findImage('.')).toEqual(
        [ './icons/chrome/maximize_icon.png',
            './icons/chrome/minimize_icon.png',
            './icons/chrome/x_close_icon.png',
            './maximize_icon - Copy (2).jpg',
            './maximize_icon - Copy.jpeg',
            './maximize_icon.png' ]
    )
})

test('Edit File, check if file is not exist before edit',()=>{
    if(fs.existsSync('result.txt')) {
        fs.unlinkSync('result.txt')
    }
    expect(fs.existsSync('result.txt')).toBeFalsy()
})

test('Edit File, check if file is exist after edit',()=>{
    editFile('sample.txt','result.txt')
    expect(fs.existsSync('result.txt')).toBeTruthy()
})