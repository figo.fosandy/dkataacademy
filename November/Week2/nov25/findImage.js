const glob=require('glob')
const pathArg=process.argv[2]
const findImage=(pathParameter)=>{
    const path=pathArg?pathArg:pathParameter
    return glob.sync(`${path}/**/*.{jpg,png,jpeg}`,{})
}

module.exports=findImage

if(pathArg){
    console.log(findImage())
}