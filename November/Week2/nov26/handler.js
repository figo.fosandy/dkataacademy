const person=require('./components/academy')
const mawar=new person.Trainee('Mawar','Melati',19,'Female','Single','Police','Jakarta','Reading','Javascipt')
const ijah=new person.Trainee('Ijah','Melati',17,'Female','Single','Neet','Bogor','Watching','C++')

const trainees=JSON.stringify([mawar,ijah])

const getTime=time=>{
    const convertedTime=new Date(time)
    return JSON.stringify({
        hour: convertedTime.getHours(),
        minute: convertedTime.getMinutes(),
        second: convertedTime.getSeconds()
    })
}

module.exports={trainees,getTime}