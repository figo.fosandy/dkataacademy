const downloadImage=require('./downloadImage')
let server=require('./server')
const fs=require('fs')
const axios=require('axios')
const {trainees}=require('./handler')

beforeAll(()=>{
    server.listen(1126)
})

afterAll(()=>{
    server.close()
})

test('Download image',async ()=>{
    const apiUrl='https://xkcd.com/info.0.json'
    axios.get(apiUrl)
        .then(response=>downloadImage(response))
        .then(imageName=>{
            expect(fs.existsSync(`comics/${imageName}`)).toBeTruthy()
    })
})

test(`Response status code for path '/' is 200`,async ()=>{
    const apiUrl='http://localhost:1126'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/' is 'This is a root route'`,async ()=>{
    const apiUrl='http://localhost:1126'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toBe('This is a root route')
        })
})

test(`Response statusCode for path '/random' is 404`,async ()=>{
    const apiUrl='http://localhost:1126/random'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(404)
        })
})

test(`Response result message for path '/random' is 'no route for path /random'`,async ()=>{
    const apiUrl='http://localhost:1126/random'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.data.message).toBe('no route for path /random')
        })
})

test(`Response status code for path '/api/v1/radha' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/radha'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/api/v1/radha' is 'Hi, i am Radha'`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/radha'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toBe('Hi, i am Radha')
        })
})

test(`Response status code for path '/api/v1/trainees' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/trainees'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/api/v1/trainees' is list of trainee`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/trainees'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toEqual(JSON.parse(trainees))
        })
})

test(`Response status code for path '/api/v1/parsetime' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?time=1' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=1'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?random=9:34 PM 11/29/2019' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?random=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?time=9:34 PM 11/29/2019' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/api/v1/parsetime?time=9:34 PM 11/29/2019' is object with property hour,minute,second`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toEqual({
                hour:21,
                minute:34,
                second:0
            })
        })
})