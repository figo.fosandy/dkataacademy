class Person{
    constructor(first,last,age,gender,status,job,address){
        this.name=first+' '+last
        this.age=age
        this.gender=gender
        this.status=status
        this.job=job
        this.address=address
    }
    introduce(){
        return `Hi, I am ${this.name}, from ${this.address}`
    }
}

class Trainer extends Person{
    constructor(first,last,age,gender,status,job,address,specialist,certificate){
        super(first,last,age,gender,status,job,address)
        this.specialist=specialist
        this.certificate=certificate
    }
    introduce(){
        return `${super.introduce()}, my specialize is ${this.specialist}`
    }
}

class Trainee extends Person{
    constructor(first,last,age,gender,status,job,address,interest,skill){
        super(first,last,age,gender,status,job,address)
        this.interest=interest
        this.skill=skill
    }
    introduce(){
        return `${super.introduce()}, my interest is ${this.interest}`
    }
}

module.exports={Trainee,Trainer}