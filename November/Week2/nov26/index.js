const downloadImage=require('./downloadImage')
const server=require('./server')
const port=1126
const axios=require('axios')
const apiUrl='https://xkcd.com/info.0.json'

axios.get(apiUrl)
    .then(response=>downloadImage(response))

server.listen(port)
console.log(`Server listening on port ${port}`)