let academy=require('./academy')

let mrBudi= new academy.Trainer('Budi','Santoso',25,'Male','Married','Software Engineer','Jakarta','React Native','RN')
let andi=new academy.Trainee('Andi','Nugroho',19,'Male','Single','Student','Bogor','Sleeping','Javascript')

test('trainer profile',()=>{
    expect(mrBudi).toEqual({
        name:'Budi Santoso',
        age:25,
        gender:'Male',
        status:'Married',
        job:'Software Engineer',
        address:'Jakarta',
        specialist:'React Native',
        certificate:'RN'
    })
})

test('trainee profile',()=>{
    expect(andi).toEqual({
        name:'Andi Nugroho',
        age:19,
        gender:'Male',
        status:'Single',
        job:'Student',
        address:'Bogor',
        interest:'Sleeping',
        skill:'Javascript'
    })
})

test('trainer introduce',()=>{
    expect(mrBudi.introduce()).toBe('Hi, I am Budi Santoso, from Jakarta, my specialize is React Native')
})

test('trainee introduce',()=>{
    expect(andi.introduce()).toBe('Hi, I am Andi Nugroho, from Bogor, my interest is Sleeping')
})