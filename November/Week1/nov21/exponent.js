const power=(num1,num2)=>{
    return num1**num2
}
const square=(num)=>{
    return power(num,2)
}
const cube=(num)=>{
    return power(num,3)
}

module.exports={power,square,cube}