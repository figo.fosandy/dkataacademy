let city=require('./cities')

const cities=['Jakarta','Bogor','Depok','Tangerang','Bekasi']

test('cities map',()=>{
    expect(city.map(cities)).toEqual([7,5,5,9,6])
})

test('cities filter',()=>{
    expect(city.filter(cities)).toEqual([7,9])
})

test('cities length',()=>{
    expect(city.length(cities)).toBe(2)
})

test('cities sort',()=>{
    expect(city.sort(cities)).toEqual(['Tangerang','Jakarta','Bekasi','Bogor','Depok'])
})

test('cities reduce',()=>{
    expect(city.reduce(cities)).toBe(32)
})