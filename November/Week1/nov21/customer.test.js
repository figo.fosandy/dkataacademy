const store=require('./customer')

let myStore=store()

test(('customer initial customer'),()=>{
    expect(myStore.getCustomer()).toBe(0)
})

test('customer coming increment',()=>{
    let temp=myStore.getCustomer()
    myStore.coming()
    expect(myStore.getCustomer()).toBe(temp+1)
})

test('customer leaving decrement',()=>{
    let temp=myStore.getCustomer()
    myStore.leaving()
    expect(myStore.getCustomer()).toBe(temp-1)
})