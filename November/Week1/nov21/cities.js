const map=(cities)=>{
    return cities.map(c=>c.length)
}

const filter=(cities)=>{
    return map(cities).filter(c=>c>6)
}

const length=(cities)=>{
    return filter(cities).length
}

const sort=(cities)=>{
    return cities.sort((a,b)=>b.length-a.length)
}

const reduce=(cities)=>{
    return map(cities).reduce((a,b)=>a+b)
}

module.exports= {map,filter,length,sort,reduce}