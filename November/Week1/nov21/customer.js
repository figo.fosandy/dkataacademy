const store=()=>{
    let customer=0
    const coming=()=>{
        customer++
    }
    const leaving=()=>{
        customer--
    }
    const getCustomer=()=>{
        return customer
    }
    return {coming,leaving,getCustomer}
}

module.exports=store