const exponent=require('./exponent')

test('exponent power',()=>{
    expect(exponent.power(2,4)).toBe(16)
})

test('exponent square',()=>{
    expect(exponent.square(2)).toBe(4)
})

test('exponent cube',()=>{
    expect(exponent.cube(2)).toBe(8)
})