const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it,before,after}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')
const {exec}=require('child_process')

const auth={
    Authorization:`Basic ${btoa('mawar:mawarmelati')}`
}

const payload={
	"name":"tiny",
	"breed":"unknown",
	"colour":"silver",
	"age":1.1,
	"next_checkup":"2020-01-01",
	"vaccinations":[
		"polio",
		"tetanus"
	]
}

describe('POST',()=>{
    let server

    before(async()=>{
        exec(`redis-server`)
    })
    
    beforeEach(async ()=>{
        exec(`redis-cli flushdb`)
        server=await start('mongodb://localhost/test')
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/pets" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            payload
        })
        expect(res.statusCode).to.equal(401)
    })
    
    it('response statusCode for path "/pets" is 400 with invalid payload',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload:Object.assign({id:1},payload)
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 201',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload
        })
        expect(res.statusCode).to.equal(201)
    })
    
    it('response statusCode for path "/pets" is 409 with conflict payload',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload
        })
        expect(res.statusCode).to.equal(409)
    })
})