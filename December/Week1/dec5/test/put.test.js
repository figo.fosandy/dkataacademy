const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it,before,after}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')
const {exec}=require('child_process')

const auth={Authorization:`Basic ${btoa('mawar:mawarmelati')}`}

describe('PUT',()=>{
    let server

    before(async()=>{
        exec(`redis-server`)
    })
    
    beforeEach(async ()=>{
        exec(`redis-cli flushdb`)
        server=await start('mongodb://localhost/test')
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/pets" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(401)
    })
    
    it('response statusCode for path "/pets/100" is 404',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/100',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(404)
    })

    it('response statusCode for path "/pets" is 400 with invalid payload',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            headers:auth,
            payload:{
                sold:false
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 400 with invalid parameter',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/a',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 409 with conflict payload',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/2',
            headers:auth,
            payload:{
                id:3,
                name:'tiny'
            }
        })
        expect(res.statusCode).to.equal(409)
    })
    
    it('response statusCode for path "/pets" is 202',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(202)
    })
})