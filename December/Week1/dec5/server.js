const Hapi=require('@hapi/hapi')
const Joi=require('@hapi/joi')
const Bcrypt=require('bcrypt')
const qs=require('qs')

const CatboxRedis=require('@hapi/catbox-redis')

const users={
    mawar:{
        username:'mawar',
        password:'$2a$04$W/8mFiweurWfRkH3UI32cuilzyU/uYqFABVatPI2kScly8QrzDftS',
        name:'Mawar Melati',
        id:'123456'
    }
}

const validate=async(request,username,password)=>{
    const user=users[username]
    if(!user){
        return {credentials:null,isValid:false}
    }
    const isValid=await Bcrypt.compare(password, user.password)
    const credentials={id:user.id,name:user.name}
    return {isValid,credentials}
}

const start=async()=>{
    const {rootHandler,petsGetHandler,getDetail,petsPostHandler,petsPutHandler,petDeleteHandler, petsDetailGetHandler}=require('./handler')
    const server=Hapi.server({
        port:1202,
        host:'0.0.0.0',
        query:{
            parser:(query)=>qs.parse(query)
        },
        cache:[
            {
                name:'my_cache',
                provider:{
                    constructor:CatboxRedis,
                    options:{
                        host:'localhost',
                        port:6379,
                    }
                }
            }
        ]
    })

    server.method('getDetail',getDetail,{
        cache:{
            cache:'my_cache',
            segment:'pets',
            expiresIn:1000*1000,
            generateTimeout:2000
        }
    })
    
    await server.register(require('@hapi/basic'))

    await server.register({
        plugin:require('hapi-pino'),
        options:{
            prettyPrint:process.env.NODE_ENV!=='production',
            redact:['req.headers.authorization'],
            stream:'server.log'
        }
    })

    server.auth.strategy('simple','basic',{validate})

    server.route({
        method:'GET',
        path:'/',
        handler:rootHandler
    })
    
    server.route({
        method:'GET',
        path:'/pets',
        handler:petsGetHandler,
        options:{
            auth:'simple',
            validate:{
                query:{
                    sort:Joi.string().regex(/^[a-z_]+$/),
                    offset:Joi.number().integer().min(0),
                    limit:Joi.number().integer().min(1),
                    filter:Joi.object()
                }
            }
        }
    })

    server.route({
        method:'GET',
        path:'/pets/{id}',
        handler:petsDetailGetHandler,
        options:{
            auth:'simple',
            validate:{
                params:{
                    id:Joi.number().integer().min(1).required()
                }
            },
            log:{
                collect:true
            }
        }
    })

    server.route({
        method:'POST',
        path:'/pets',
        handler:petsPostHandler,
        options:{
            auth:'simple',
            validate:{
                payload:{
                    id:Joi.forbidden(),
                    name:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/).required(),
                    breed:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/).required(),
                    colour:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/).required(),
                    age:Joi.number().min(0).required(),
                    next_checkup:Joi.date().min(Date.now()).required(),
                    vaccinations:Joi.array().required(),
                    imageUri:Joi.string(),
                    sold:Joi.forbidden()
                }
            }
        }
    })

    server.route({
        method:'PUT',
        path:'/pets/{id}',
        handler:petsPutHandler,
        options:{
            auth:'simple',
            validate:{
                params:{
                    id:Joi.number().integer().required()
                },
                payload:{
                    id:Joi.number().integer(),
                    name:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/),
                    breed:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/),
                    colour:Joi.string().regex(/^[a-zA-Z]+[/\s]*[a-zA-Z]+$/),
                    age:Joi.number().min(0),
                    next_checkup:Joi.date().min(Date.now()),
                    vaccinations:Joi.array(),
                    imageUri:Joi.string(),
                    sold:Joi.forbidden()
                }
            }
        }
    })

    server.route({
        method:'DELETE',
        path:'/pets/{id}',
        handler:petDeleteHandler,
        options:{
            auth:'simple',
            validate:{
                params:{
                    id:Joi.number().integer().required()
                }
            }
        }
    })

    await server.start()

    process.on('unhandledRejection',(err)=>{
        console.log(err)
        process.exit(1)
    })
    
    return server
}

module.exports={start}