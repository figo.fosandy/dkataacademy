const mongoose=require('mongoose')
const config=require('./config.json')
mongoose.connect(config.mongodbUrl,{useNewUrlParser:true,useUnifiedTopology:true})
mongoose.set('debug', true)

module.exports=mongoose