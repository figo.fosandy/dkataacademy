const mongoose=require('./moongoseConnection')
const db=mongoose.connection
db.on('error',console.error.bind(console,'connection error'))
db.once('open',()=>console.log('Connected'))

const Pet=mongoose.model('Pet',{
    id:Number,
    name:String,
    breed:String,
    colour:String,
    age:Number,
    next_checkup:Date,
    vaccinations:[String],
    sold:Boolean,
    imageUri:String
})

const fields=[]
Pet.schema.eachPath(field=>fields.push(field))

const notFoundResponse={
    statusCode:404,
    error:'Not Found',
    message:'Not found'
}

const badRequestResponse={
    statusCode:400,
    error:'Bad Request',
    message:'Invalid request input'
}

const conflictResponse={
    statusCode:409,
    error:'Conflict',
    message:'Conflict request input'
}

const rootHandler=(request,h)=>{
    request.logger.info(`In handler ${request.path}`)
    return h.response('This is a root route').code(200)
}

const petsGetHandler=async (request,h)=>{
    const query=request.query
    const isEmpty=JSON.stringify(query)=='{}'
    if(isEmpty){
        const result= await Pet.find({sold:false},{_id:0,__v:0,sold:0}).lean()
        return h.response(result).code(200)
    } else {
        let sortBy=query.sort?query.sort:'name'
        let offset=query.offset?parseInt(query.offset):0
        let limit=query.limit?parseInt(query.limit):10
        let filter=query.filter?query.filter:{sold:false}
        if(!fields.some(field=>field==sortBy)){
            return h.response(badRequestResponse).code(400)
        }
        const result=await Pet.find(filter,{_id:0,__v:0})
            .sort({[sortBy]:1})
            .skip(offset)
            .limit(limit)
            .lean()
        return h.response(result).code(200)
    }
}

const getDetail=async (petId)=>{
    return await Pet.findOne({id:petId,sold:false},{_id:0,__v:0}).lean()
}

const petsDetailGetHandler=async (request,h)=>{
    const petId=request.params.id
    const detailCache=await request.server.methods.getDetail(petId)
    if(!detailCache){   
        return h.response(notFoundResponse).code(404)
    }
    return h.response(detailCache).code(200)
}

const petsPostHandler=async (request,h)=>{
    const pets=await Pet.aggregate([
        {
            $group:{
                _id:'max',
                maxId:{
                    $max:'$id',
                },
                names:{
                    $push:'$name'
                }
            }
        }
    ])  
    const nameAvailability=pets[0].names.some(name=>name==request.payload.name)
    if(nameAvailability){
        return h.response(conflictResponse).code(409)
    }
    let nextId=pets[0].maxId+1
    Object.assign(request.payload,{id:nextId,sold:false})
    await Pet.insertMany([request.payload])
    return h.response(request.payload).code(201)
}

const petsPutHandler=async (request,h)=>{
    let id=request.params.id
    if(request.payload.id&&request.payload.id!=id){
        return h.response(conflictResponse).code(409)
    }
    let filter={
        id:id,
        sold:false
    }
    if(request.payload.name){
        Object.assign(filter,{name:request.payload.name})
    }
    const updatedPet=await Pet.findOneAndUpdate(filter,request.payload,{new:true,projection:{_id:0,__v:0}}).lean()
    await request.server.methods.getDetail.cache.drop(id)
    if(!updatedPet){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(updatedPet).code(202)
}

const petDeleteHandler=async (request,h)=>{
    let id=request.params.id
    const deletedPet=await Pet.findOneAndUpdate({id:id,sold:false},{sold:true},{new:true,projection:{_id:0,__v:0}}).lean()
    if(!deletedPet){
        return h.response(notFoundResponse).code(404)
    }
    return h.response({
        statusCode:202,
        error:'',
        message:'Deleted'
    }).code(202)
}

module.exports={rootHandler,petsGetHandler,petsDetailGetHandler,petsPostHandler,petsPutHandler,petDeleteHandler,getDetail}