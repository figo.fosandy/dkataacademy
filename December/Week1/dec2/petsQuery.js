const pets=require('./pets.json')

pets.forEach(pet => {
    Object.assign(pet,{sold:false})
})

const getsPetSortOffsetLimit=(sortBy,offset,limit,option)=>{
    const petsFiltered=getPets(option)
    if(typeof petsFiltered[0][sortBy] == 'string') {
        result=petsFiltered.sort((a,b)=>a[sortBy]>b[sortBy])
    } else if(typeof petsFiltered[0][sortBy] == 'number') {
        result=petsFiltered.sort((a,b)=>a[sortBy]-b[sortBy])
    }
    return petsFiltered.slice(offset,offset+limit)
}

const getPets=(option)=>{
    if(option=='unsold'){
        return pets.filter(pet=>pet.sold==false)
    } else if(option=='all'){
        return pets
    } else if(option=='sold'){
        return pets.filter(pet=>pet.sold==true)
    }
}

const getPetDetail=(id,option)=>{
    const petsFiltered=getPets(option)
    return petsFiltered.filter(pet=>pet.id==id)[0]
}

const addPet=(pet)=>{
    let ids=pets.map(pet=>pet.id)
    let lastId=Math.max(...ids)    
    let newPet=Object.assign({id:lastId+1,sold:false},pet)
    pets.push(newPet)
    return newPet
}

const updatePet=(id,pet)=>{
    Object.assign(pets.find(pet=>pet.id==id),pet)
    return pets.find(pet=>pet.id==id)
}

const deletePet=(id)=>{
    let petFound=pets.findIndex(pet=>pet.id==id)
    if(petFound<0){
        return false
    }
    if (pets[petFound].sold==true){
        return false
    } else {
        updatePet(id,{sold:true})
        return true
    }
}

module.exports={getsPetSortOffsetLimit,getPets,getPetDetail,addPet,updatePet,deletePet}