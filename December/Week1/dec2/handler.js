const {addPet,getPetDetail,getPets,getsPetSortOffsetLimit,updatePet,deletePet}=require('./petsQuery')

const notFound={
    statusCode:404,
    error:'Not Found',
    message:'Not found'
}

const rootHandler=(request,h)=>{
    return h.response('This is a root route').code(200)
}

const petsGetHandler=(request,h)=>{
    if(!request.query.sort&&!request.query.offset&&!request.query.limit&&!request.query.filter){
        const petList=getPets('unsold')
        if(petList.length) {
            return h.response(getPets('unsold')).code(200)
        } else{
            return h.response(notFound).code(404)
        }
    }
    let sortBy=request.query.sort?request.query.sort:'id'
    let offset=request.query.offset?parseInt(request.query.offset):0
    let limit=request.query.limit?parseInt(request.query.limit):10
    let filter=request.query.filter?request.query.filter:'unsold'
    const options=['all','sold','unsold']
    if(!options.some(option=>option==filter)){
        return h.response({
            statusCode:400,
            error:'Bad Request',
            message:'Invalid request query input'
        }).code(400)
    }
    if(!getPets(filter).length){
        return h.response(notFound).code(404)
    }
    let id=getPets(filter)[0].id
    if(!getPetDetail(id,filter)[sortBy]){
        return h.response({
            statusCode:400,
            error:'Bad Request',
            message:'Invalid request query input'
        }).code(400)
    }
    const result=getsPetSortOffsetLimit(sortBy,offset,limit,filter)
    if(!result.length){
        return h.response(notFound).code(404)
    }
    return h.response(result).code(200)
}

const petsDetailGetHandler=(request,h)=>{
    const id=request.params.id
    const result=getPetDetail(id,'unsold')
    if(!result){
        return h.response(notFound).code(404)
    }
    return h.response(result).code(200)
}

const petsPostHandler=(request,h)=>{
    let names=getPets('all').map(pet=>pet.name)
    let newPetName=request.payload.name
    let nameAvailability=names.some(name=>name==newPetName)
    if(nameAvailability) {
        return h.response({
            statusCode:409,
            error:'Conflict',
            message:'Name is conflict'
        }).code(409)
    }
    return h.response(addPet(request.payload)).code(201)
}

const petsPutHandler=(request,h)=>{
    let id=request.params.id
    let detail=getPetDetail(id,'unsold')
    if(!detail){
        return h.response(notFound).code(404)
    }
    let name=detail.name
    if((request.payload.name&&request.payload.name!=name)||(request.payload.id&&request.payload.id!=id)){
        return h.response({
            statusCode:409,
            error:'Conflict',
            message:'Id or name is conflict'
        }).code(409)
    }
    return h.response(updatePet(id,request.payload)).code(202)
}

const petDelete=async (request,h)=>{
    let id=request.params.id
    if(deletePet(id)){
        return h.response({
            statusCode:202,
            message:'Deleted'
        }).code(202)
    } else {
        return h.response(notFound).code(404)
    }
}

module.exports={rootHandler,petsGetHandler,petsDetailGetHandler,petsPostHandler,petsPutHandler,petDelete}