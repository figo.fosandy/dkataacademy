'use strict';

const Bcrypt = require('bcrypt');
const Hapi = require('@hapi/hapi');

const users = {
    mawar: {
        username: 'mawar',
        password: '$2a$04$rY5Q0tX/sl.PlVd9oua0s.wOW.4Lmq4fYapbLYtjnJZ9G5uaGFVF.',   // 'secret'
        name: 'Mawar Melati',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const start = async () => {

    const server = Hapi.server({ port: 4000 });

    await server.register(require('@hapi/basic'));

    server.auth.strategy('simple', 'basic', { validate });

    server.route({
        method: 'GET',
        path: '/',
        options: {
            auth: 'simple'
        },
        handler: function (request, h) {
            return 'welcome';
        }
    });


    await server.start();
    return server
};

module.exports={start}

// start()
//     .then((server)=>{
//         console.log('server running at: ' + server.info.uri);
//     })
//     .catch((err)=>{
//         console.error(err);
//         process.exit(1)
//     })