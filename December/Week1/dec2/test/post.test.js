const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')

const auth={Authorization:`Basic ${btoa('mawar:mawarmelati')}`}
const payload={
	"name":"tiny",
	"breed":"unknown",
	"colour":"silver",
	"age":1.1,
	"next_checkup":"2019-12-02",
	"vaccinations":[
		"polio",
		"tetanus"
	]
}

describe('POST',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/pets" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            payload
        })
        expect(res.statusCode).to.equal(401)
    })
    
    it('response statusCode for path "/pets" is 400 with invalid payload',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload:Object.assign({id:1},payload)
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 201',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload
        })
        expect(res.statusCode).to.equal(201)
    })
    
    it('response statusCode for path "/pets" is 409 with conflict payload',async()=>{
        const res=await server.inject({
            method:'POST',
            url:'/pets',
            headers:auth,
            payload
        })
        expect(res.statusCode).to.equal(409)
    })
})