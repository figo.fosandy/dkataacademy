const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')

const auth={
    Authorization:`Basic ${btoa('mawar:mawarmelati')}`
}

describe('GET',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/" is 200',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.statusCode).to.equal(200)
    })
    
    it('response result for path "/" is "This is a root route"',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/'
        })
        expect(res.result).to.equal("This is a root route")
    })
    
    it('response statusCode for path "/pets" is 401 without authorization',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets'
        })
        expect(res.statusCode).to.equal(401)
    })

    it('response statusCode for path "/pets" is 200 with authorization',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets',
            headers:auth
        })
        expect(res.statusCode).to.equal(200)
    })

    it('response statusCode for path "/pets?offset=7" is 404',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets?offset=7',
            headers:auth
        })
        expect(res.statusCode).to.equal(404)
    })

    it('response statusCode for path "/pets?sort=name1" is 400',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets?sort=name1',
            headers:auth
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets/{id}" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/1'
        })
        expect(res.statusCode).to.equal(401)
    })

    it('response statusCode for path "/pets/{id}" is 400 with invalid request',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/a',
            headers:auth
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets/{id}" is 200 with right request',async()=>{
        const res=await server.inject({
            method:'GET',
            url:'/pets/1',
            headers:auth
        })
        expect(res.statusCode).to.equal(200)
    })
})