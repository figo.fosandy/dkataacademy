const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')

const auth={Authorization:`Basic ${btoa('mawar:mawarmelati')}`}

describe('DELETE',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/pets" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'DELETE',
            url:'/pets/1'
        })
        expect(res.statusCode).to.equal(401)
    })
    
    it('response statusCode for path "/pets/100" is 404',async()=>{
        const res=await server.inject({
            method:'DELETE',
            url:'/pets/100',
            headers:auth
        })
        expect(res.statusCode).to.equal(404)
    })

    it('response statusCode for path "/pets" is 400 with invalid parameter',async()=>{
        const res=await server.inject({
            method:'DELETE',
            url:'/pets/a',
            headers:auth
        })
        expect(res.statusCode).to.equal(400)
    })
    
    it('response statusCode for path "/pets" is 202',async()=>{
        const res=await server.inject({
            method:'DELETE',
            url:'/pets/2',
            headers:auth
        })
        expect(res.statusCode).to.equal(202)
    })
})