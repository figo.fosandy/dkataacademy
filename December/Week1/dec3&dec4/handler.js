const mongoose=require('./moongoseConnection')

const db=mongoose.connection
db.on('error',console.error.bind(console,'connection error'))
db.once('open',()=>console.log('Connected'))

const Pet=mongoose.model('Pet',{
    id:Number,
    name:String,
    breed:String,
    colour:String,
    age:Number,
    date:Date,
    vaccinations:[String],
    sold:Boolean
})

const fields=[]
Pet.schema.eachPath(field=>fields.push(field))

const notFoundResponse={
    statusCode:404,
    error:'Not Found',
    message:'Not found'
}

const badRequestResponse={
    statusCode:400,
    error:'Bad Request',
    message:'Invalid request input'
}

const conflictResponse={
    statusCode:409,
    error:'Conflict',
    message:'Conflict request input'
}

const rootHandler=(request,h)=>{
    return h.response('This is a root route').code(200)
}

const petsGetHandler=async (request,h)=>{
    const query=request.query
    const isEmpty=JSON.stringify(query)=='{}'
    if(isEmpty){
        const result= await Pet.find({sold:false},{_id:0,__v:0,sold:0}).lean()
        return h.response(result).code(200)
    } else {
        let sortBy=query.sort?query.sort:'name'
        let offset=query.offset?parseInt(query.offset):0
        let limit=query.limit?parseInt(query.limit):10
        let filter=query.filter?query.filter:{sold:false}
        if(!fields.some(field=>field==sortBy)){
            return h.response(badRequestResponse).code(400)
        }
        const result=await Pet.find(filter,{_id:0,__v:0})
            .sort({[sortBy]:1})
            .skip(offset)
            .limit(limit)
            .lean()
        return h.response(result).code(200)
    }
}

const petsDetailGetHandler=async (request,h)=>{
    const id=request.params.id
    const result=await Pet.find({id:id,sold:false}).lean()
    if(!result[0]){
        return h.response(notFoundResponse).code(404)
    }
    return h.response(result[0]).code(200)
}

const petsPostHandler=async (request,h)=>{
    const pets=await Pet.aggregate([
        {
            $group:{
                _id:'max',
                maxId:{
                    $max:'$id',
                },
                names:{
                    $push:'$name'
                }
            }
        }
    ])  
    const nameAvailability=pets[0].names.some(name=>name==request.payload.name)
    if(nameAvailability){
        return h.response(conflictResponse).code(409)
    }
    let nextId=pets[0].maxId+1
    Object.assign(request.payload,{id:nextId,sold:false})
    await Pet.insertMany([request.payload])
    return h.response(request.payload).code(201)
}

const petsPutHandler=async (request,h)=>{
    let id=request.params.id
    const detail= await Pet.find({id:id,sold:false})
    if(!detail.length){
        return h.response(notFoundResponse).code(404)
    }
    let name=detail.name
    if((request.payload.name&&request.payload.name!=name)||(request.payload.id&&request.payload.id!=id)){
        return h.response(conflictResponse).code(409)
    }
    const updatedPet=await Pet.findOneAndUpdate({id:id,sold:false},request.payload,{new:true,}).lean()
    return h.response(updatedPet).code(202)
}

const petDeleteHandler=async (request,h)=>{
    let id=request.params.id
    const detail= await Pet.find({id:id,sold:false})
    if(!detail.length){
        return h.response(notFoundResponse).code(404)
    } else {
        await Pet.findOneAndUpdate({id:id,sold:false},{sold:true},{new:true}).lean()
        return h.response({
            statusCode:202,
            error:'',
            message:'Deleted'
        }).code(202)
    }
}

module.exports={rootHandler,petsGetHandler,petsDetailGetHandler,petsPostHandler,petsPutHandler,petDeleteHandler}