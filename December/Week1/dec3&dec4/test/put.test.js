const Lab=require('@hapi/lab')
const {expect}=require('@hapi/code')
const {afterEach,beforeEach,describe,it}=exports.lab=Lab.script()
const {start}=require('../server')
const btoa=require('btoa')

const auth={Authorization:`Basic ${btoa('mawar:mawarmelati')}`}

describe('PUT',()=>{
    let server

    beforeEach(async ()=>{
        server=await start()
    })

    afterEach(async ()=>{
        await server.stop()
    })

    it('response statusCode for path "/pets" is 401 without authorize',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(401)
    })
    
    it('response statusCode for path "/pets/100" is 404',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/100',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(404)
    })

    it('response statusCode for path "/pets" is 400 with invalid payload',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            headers:auth,
            payload:{
                sold:false
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 400 with invalid parameter',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/a',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(400)
    })

    it('response statusCode for path "/pets" is 409 with conflict payload',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            headers:auth,
            payload:{
                name:'tiny'
            }
        })
        expect(res.statusCode).to.equal(409)
    })
    
    it('response statusCode for path "/pets" is 202',async()=>{
        const res=await server.inject({
            method:'PUT',
            url:'/pets/1',
            headers:auth,
            payload:{
                age:10
            }
        })
        expect(res.statusCode).to.equal(202)
    })
})