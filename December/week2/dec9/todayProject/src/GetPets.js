import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'
import axios from 'axios'
import { Pet } from '../components/PetComponent'

export default class GetPets extends Component {
    componentDidMount() {
        axios.get(`http://20.4.12.29:1202/pets`, {
            auth: {
                username: 'mawar',
                password: 'mawarmelati'
            }
        })
            .then(res => {
                const pets = res.data;
                this.setState({ pets: pets, loading: false });
            })
    }
    state = {
        loading: true,
        pets: []
    }

    getPets = () => {
        if (this.state.loading) {
            return (<View>
                <Text>Loading ...</Text>
            </View>)
        }
        return <FlatList
            data={this.state.pets}
            renderItem={({ item }) => Pet(item)}
            keyExtractor={({ id }) => id}
        />
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text>My Pets</Text>
                {this.getPets()}
            </View>
        );
    }
}