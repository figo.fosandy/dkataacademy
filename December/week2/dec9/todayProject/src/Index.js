import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

export default class Index extends Component {
  render() {
    return (
      <View style={{flex:1}}>
        <Image source={
          {uri:'https://animaljusticeaustralia.files.wordpress.com/2017/07/part_1490851882089.jpeg'}}
          style={{
            flex:1
          }} resizeMode='contain'
          />
        <View style={{flex:1,flexDirection:'row',padding:10}}>
          <View style={{flex:1}}>
            <Text>Name : Anonymous</Text>
            <Text>Breed : Unknown</Text>
            <Text>Next_Checkup : 2020-01-01</Text>
          </View>
          <View style={{flex:1}}>
            <Text>Age : 12</Text>
            <Text>Colours : Light Brown</Text>
          </View>
        </View>
      </View>
    );
  }
}