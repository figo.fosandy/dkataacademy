import React,{Component} from 'react'
import {View,Text} from 'react-native'

function Pet(props){
    return (<View style={{flex:1,flexDirection:'row',padding:10,borderRadius:4,borderWidth:0.5,borderColor:'black',marginTop:10}}>
                <View style={{flex:1}}>
                    <Text>Name : {props.name}</Text>
                    <Text>Breed : {props.breed}</Text>
                    <Text>Next_Checkup : {props.next_checkup}</Text>
                </View>
                <View style={{flex:1}}>
                    <Text>Age : {props.age}</Text>
                    <Text>Colour : {props.colour}</Text>
                </View>
            </View>)
}
export {Pet}