import React, { Component } from 'react'
import { Text, View, FlatList} from 'react-native'
import axios from 'axios'
import { Pet } from '../components/PetComponent'

export default class PetList extends Component {
    static navigationOptions={
        title:'Pet List'
    }
    componentDidMount() {
        axios.get(`http://192.168.168.2:1202/pets`, {
            auth: {
                username: 'mawar',
                password: 'mawarmelati'
            }
        })
            .then(res => {
                const pets = res.data;
                this.setState({ pets: pets, loading: false });
            })
    }
    state = {
        loading: true,
        pets: []
    }

    getPets = () => {
        if (this.state.loading) {
            return (<View>
                <Text>Loading ...</Text>
            </View>)
        }
        return <FlatList
            data={this.state.pets}
            renderItem={({ item }) => Pet(item,this.props.navigation)}
            keyExtractor={({ id }) => id}
        />
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text>My Pets</Text>
                {this.getPets()}
            </View>
        );
    }
}