import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import PetList from './PetList'
import PetDetail from './PetDetail'

const MainNavigator=createStackNavigator({
    PetList:{screen:PetList},
    PetDetail:{screen:PetDetail}
})

const App=createAppContainer(MainNavigator)

export default App