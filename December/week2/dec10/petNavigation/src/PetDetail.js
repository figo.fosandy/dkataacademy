import React, { Component } from 'react'
import { Text, View, Image, TextInput, FlatList, TouchableOpacity } from 'react-native'
import DatePicker from 'react-native-datepicker'
import axios from 'axios'

export default class PetDetail extends Component {
  constructor(props){
    super(props)
    const {getParam}=this.props.navigation
    this.state={
      pet:{
        id:getParam('id'),
        name:getParam('name'),
        breed:getParam('breed'),
        next_checkup:getParam('next_checkup'),
        age:getParam('age'),
        colour:getParam('colour'),
        imageUri:getParam('imageUri'),
        vaccinations:getParam('vaccinations')
      },
      updateNextCheckUp:false,
      updateAge:false,
      addVaccinations:false,
      updateImageUri:false
    }
  }
  static navigationOptions={
    title:`Pet Detail`
  }
  updateNextCheckup(){
    if(this.state.updateNextCheckUp){
      return (
        <DatePicker
          style={{width:'100%',padding:10}}
          date={this.state.pet.next_checkup}
          mode="date"
          format="YYYY-MM-DD"
          minDate={new Date().toISOString()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={date => {
            this.setState({ pet:{...this.state.pet,next_checkup:date}});
          }}
        />
      )
    }
    return
  }
  updateAge(){
    if(this.state.updateAge){
      return (
        <TextInput
          keyboardType='numeric'
          value={this.state.pet.age.toString()}
          onChangeText={(text)=>this.setState({pet:{...this.state.pet,age:text}})}
        />
      )
      return
    }
  }

  updateVaccinations(item,index){
    if(this.state[`updateVaccinations${index}`]){
      let temp=this.state.pet.vaccinations
      return (
        <TextInput
          value={this.state.pet.vaccinations[index]}
          onChangeText={(text)=>{
            temp[index]=text
            this.setState({pet:{...this.state.pet,vaccinations:temp}})
          }}
          onEndEditing={(e)=>{
            this.setState({[`updateVaccinations${index}`]:!this.state[`updateVaccinations${index}`]})
          }}
        />
      )
    }
    return (
      <Text
        onPress={()=>this.setState({[`updateVaccinations${index}`]:!this.state[`updateVaccinations${index}`]})}
      >{item}</Text>
    )
  }

  addVaccinations(){
    if(this.state.addVaccinations){
      let temp=this.state.pet.vaccinations
      return(
        <TextInput
          style={{paddingLeft:10}}
          placeholder="Input The New Vaccination"
          onEndEditing={(e)=>{
            temp.push(e.nativeEvent.text)
            this.setState({addVaccinations:!this.state.addVaccinations,pet:{...this.state.pet,vaccinations:temp}})
          }}
        />
      )
    }
    return
  }

  updateImageUri(){
    if(this.state.updateImageUri){
      return(
        <TextInput
          value={this.state.pet.imageUri}
          onChangeText={(text)=>{this.setState({pet:{...this.state.pet,imageUri:text}})
          }}
          onEndEditing={(e)=>{
            this.setState({updateImageUri:!this.state.updateImageUri})
          }}
      />
      )
    }
    return
  }

  render() {
    return (
      <View style={{flex:1}}>
        <View style={{flex:1}}>
          <TouchableOpacity
            style={{flex:1}}
            onPress={()=>this.setState({updateImageUri:!this.state.updateImageUri})}
          >
            <Image source={{uri:this.state.pet.imageUri}}
              style={{flex:1}}
              resizeMode='contain'
              />
          </TouchableOpacity>
          {this.updateImageUri()}
        </View>
        <View style={{flex:1,flexDirection:'row',padding:10}}>
          <View style={{flex:1}}>
            <Text>Name : {this.state.pet.name}</Text>
            <Text>Breed : {this.state.pet.breed}</Text>
            <Text
              onPress={()=>this.setState({updateNextCheckUp:!this.state.updateNextCheckUp})}
            >Next_Checkup : {this.state.pet.next_checkup}</Text>
            {this.updateNextCheckup()}
            <Text
              onPress={()=>this.setState({updateAge:!this.state.updateAge})}
            >Age : {this.state.pet.age}</Text>
            {this.updateAge()}
            <Text>Colours : {this.state.pet.colour}</Text>
          </View>
          <View style={{flex:1}}>
            <Text
              onPress={()=>this.setState({addVaccinations:!this.state.addVaccinations})}
            >Vaccinations :</Text>
            {this.addVaccinations()}
            <FlatList
              style={{paddingLeft:10}}
              data={this.state.pet.vaccinations}
              keyExtractor={({index})=>index}
              renderItem={({item,index})=>this.updateVaccinations(item,index)}
            />
          </View>
        </View>
      </View>
    );
  }
  componentWillUnmount(){
    axios.put(`http://192.168.168.2:1202/pets/${this.state.pet.id}`,this.state.pet,{
      auth:{
        username:'mawar',
        password:'mawarmelati'
      }
    })
  }
}